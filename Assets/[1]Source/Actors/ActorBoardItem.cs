﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pixeye.Actors;
using Sirenix.OdinInspector;

namespace Test.Math3Like.Source
{
    public class ActorBoardItem : Actor
    {
        [FoldoutGroup("Setup"), HideLabel, Title("Component Transform")]
        public ComponentTransform componentTransform;
        [FoldoutGroup("Setup"), HideLabel, Title("Component Board Item")]
        public ComponentBoardItem componentBoardItem;
        [FoldoutGroup("Setup"), HideLabel, Title("Component GUID")]
        public ComponentGUID componentGUID;

        protected override void Setup()
        {
            entity.Set(componentTransform);
            entity.Set(componentBoardItem);
            entity.Set(componentGUID);
        }
    }
}
