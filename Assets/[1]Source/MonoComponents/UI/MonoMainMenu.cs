﻿using Beebyte.Obfuscator;
using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Test.Math3Like.Source
{
    public class MonoMainMenu : MonoCached
    {
        protected override void Setup()
        {
            ProcessorSoundPool.Default.PlaySound(DataGameMain.Default.clipsOnMainMenu.Random(), 1f).loop = true;
        }
        [SkipRename]
        public void ButtonStart()
        {
            ProcessorScene.To("Scene Game");
        }
        [SkipRename]
        public void ButtonExit()
        {
            Application.Quit();
        }
    }
}
