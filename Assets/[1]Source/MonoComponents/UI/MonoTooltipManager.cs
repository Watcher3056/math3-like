﻿using UnityEngine;
using Pixeye.Actors;
using Sirenix.OdinInspector;

namespace Test.Math3Like.Source
{
    public class MonoTooltipManager : MonoCached, ITick
    {
        [Title("RESOURCES")]
        [FoldoutGroup("Setup")]
        public GameObject tooltipObject;
        [FoldoutGroup("Setup")]
        public GameObject tooltipContent;
        [FoldoutGroup("Setup")]
        public RectTransform tooltipHelper;

        [Title("SETTINGS")]
        [FoldoutGroup("Setup")]
        [PropertyRange(0.05f, 0.5f)] public float tooltipSmoothness = 0.1f;
        // public Vector3 tooltipPosition = new Vector3(120, -60, 0);

        [Title("TOOLTIP BOUNDS")]
        [FoldoutGroup("Setup")]
        public int vBorderTop = -115;
        [FoldoutGroup("Setup")]
        public int vBorderBottom = 100;
        [FoldoutGroup("Setup")]
        public int hBorderLeft = 230;
        [FoldoutGroup("Setup")]
        public int hBorderRight = -210;

        private Vector2 uiPos;
        private Vector3 cursorPos;
        private RectTransform tooltipRect;
        private Vector3 contentPos = new Vector3(0, 0, 0);
        private Vector3 tooltipVelocity = Vector3.zero;

        void Start()
        {
            tooltipObject.SetActive(true);
            tooltipRect = tooltipObject.GetComponent<RectTransform>();
            // tooltipContent.transform.localPosition = tooltipPosition;
            contentPos = new Vector3(vBorderTop, hBorderLeft, 0);
        }
        public void Tick(float delta)
        {
            cursorPos = Input.mousePosition;
            cursorPos.z = tooltipHelper.position.z;
            tooltipRect.position = MonoCamera.Default.camera.ScreenToWorldPoint(cursorPos);
            uiPos = tooltipRect.anchoredPosition;
            tooltipContent.transform.localPosition = Vector3.SmoothDamp(tooltipContent.transform.localPosition, contentPos, ref tooltipVelocity, tooltipSmoothness);
            CheckForBounds();
        }
        public void CheckForBounds()
        {
            if (uiPos.x <= -300)
            {
                contentPos = new Vector3(hBorderLeft, contentPos.y, 0);
            }

            if (uiPos.x >= 300)
            {
                contentPos = new Vector3(hBorderRight, contentPos.y, 0);
            }

            if (uiPos.y <= -250)
            {
                contentPos = new Vector3(contentPos.x, vBorderBottom, 0);
            }

            if (uiPos.y >= 250)
            {
                contentPos = new Vector3(contentPos.x, vBorderTop, 0);
            }
        }
    }
}