﻿using Beebyte.Obfuscator;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using static Test.Math3Like.Source.MonoPanel;
using static Test.Math3Like.Source.ProcessorGame;

namespace Test.Math3Like.Source
{
    public class MonoGameUI : MonoCached
    {
        public static MonoGameUI Default => _default;
        private static MonoGameUI _default;

        [Required]
        public MonoTopPanelManager topPanelManager;
        [Required]
        public Button buttonPlayPause;
        [Required]
        public Image imageButtonPlayPause;
        [Required]
        public Sprite spritePlay;
        [Required]
        public Sprite spritePause;
        [Required]
        public MonoPanel panelPauseMenu;
        [Required]
        public MonoPanel panelLose;
        [Required]
        public MonoPanel panelWon;

        public MonoGameUI()
        {
            _default = this;
        }
        protected override void Setup()
        {

        }
        public void UpdateView()
        {
            buttonPlayPause.interactable = ProcessorGame.Default.CurState == GameState.Play;

            imageButtonPlayPause.sprite = ProcessorGame.Default.CurState == GameState.Pause ? spritePause : spritePlay;

            MonoPanel changeTo = null;

            if (ProcessorGame.Default.CurState == GameState.Pause)
                changeTo = panelPauseMenu;
            else if (ProcessorGame.Default.CurState == GameState.Lose)
                changeTo = panelLose;
            else if (ProcessorGame.Default.CurState == GameState.Win)
                changeTo = panelWon;

            if (changeTo != null)
                topPanelManager.ChangePanelTo(changeTo);
            else
                topPanelManager.HideAllPanels(false);
            //panelPauseMenu.TogglePanel(ProcessorGame.Default.CurState == GameState.Pause);

            //panelLose.TogglePanel(ProcessorGame.Default.CurState == GameState.Lose);
            //panelWon.TogglePanel(ProcessorGame.Default.CurState == GameState.Win);
        }
        [SkipRename]
        public void ButtonTogglePause(bool arg)
        {
            ProcessorGame.Default.CurState = arg ? GameState.Pause : GameState.Play;

            UpdateView();
        }
        [SkipRename]
        public void ButtonRestartGame()
        {
            ProcessorGame.Default.Restart();

            UpdateView();
        }
        [SkipRename]
        public void ButtonReturnToMainMenu()
        {
            ProcessorScene.To("Scene Main Menu");
        }
    }
}
