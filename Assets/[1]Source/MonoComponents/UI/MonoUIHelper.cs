﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;

namespace Test.Math3Like.Source
{
    public class MonoUIHelper : MonoCached
    {
        [SkipRename]
        public void RestartGame()
        {
            ProcessorGame.Default.Restart();
        }
        [SkipRename]
        public void ChangeToScene(string sceneName)
        {
            ProcessorScene.To(sceneName);
        }
        [SkipRename]
        public void ChangeToScene(int sceneIndex)
        {
            ProcessorScene.To(sceneIndex);
        }
        [SkipRename]
        public void ExitGame()
        {
            Application.Quit();
        }
    }
}