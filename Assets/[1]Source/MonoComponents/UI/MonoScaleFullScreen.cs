﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Pixeye.Actors;
using Sirenix.OdinInspector;

namespace Test.Math3Like.Source
{
    public class MonoScaleFullScreen : MonoCached
    {
        [FoldoutGroup("Setup", Expanded = true)]
        public Transform transformToScale;
        [FoldoutGroup("Setup")]
        public Image mainImage;

        protected override void Setup()
        {
            HandleOrientationChanged();
        }
        protected override void HandleEnable()
        {
            ProcessorOrientation.OnOrientationChanged += HandleOrientationChanged;
            HandleOrientationChanged();
        }
        protected override void HandleDisable()
        {
            ProcessorOrientation.OnOrientationChanged -= HandleOrientationChanged;
        }
        private void HandleOrientationChanged()
        {
            float newScale = 0;
            newScale = (float)Screen.height /
                (((float)Screen.width / (float)mainImage.sprite.rect.width) * mainImage.sprite.rect.height);

            transformToScale.localScale = new Vector3(newScale, newScale, 1f);
        }
    }
}
