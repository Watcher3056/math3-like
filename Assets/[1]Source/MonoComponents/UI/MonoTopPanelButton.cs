﻿using UnityEngine;
using UnityEngine.EventSystems;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;

namespace Test.Math3Like.Source
{
    public class MonoTopPanelButton : MonoCached, IPointerEnterHandler, IPointerExitHandler
    {
        [Required]
        public Animator buttonAnimator;

        [ShowInInspector]
        private const string animKeyHover = "Hover";
        [ShowInInspector]
        private const string animKeyNormal = "Normal";
        protected override void Setup()
        {

        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (buttonAnimator.GetCurrentAnimatorStateInfo(0).IsName("Hover to Pressed"))
            {
                // do nothing because it's clicked
            }

            else
            {
                buttonAnimator.Play("Hover");
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (buttonAnimator.GetCurrentAnimatorStateInfo(0).IsName("Hover to Pressed"))
            {
                // do nothing because it's clicked
            }

            else
            {
                buttonAnimator.Play("Normal");
            }
        }
    }
}
