﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beebyte.Obfuscator;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Test.Math3Like.Source
{
    public class MonoEvent : MonoCached, ITick
    {
        public BetterEvent monoEvent;
        public bool activateOnStart;

        [ReadOnly]
        private float curDelay;
        [ReadOnly]
        private int curIndex;

        protected override void Setup()
        {
            ProcessorUpdate.Add(this);
            if (activateOnStart)
                Invoke();
        }
        [SkipRename] //Skip rename of this method, else we get problems on build. 
        //You must did it for all methods you would to use trough editor(in buttons, events etc.)
        public void Invoke()
        {
            while (curIndex < monoEvent.Events.Count)
            {
                if (curDelay > 0) //if delay was setted in previous entry, then we need to stop
                    return;
                monoEvent.Events[curIndex].Invoke();
                curIndex++;
            }
            curIndex = 0; //set index to 0 when all entries was passed
        }

        public void Tick(float delta)
        {
            curDelay = curDelay > 0 ? curDelay - delta : 0; //decrement time
            if (curIndex > 0 && curDelay <= 0) //if event was stoped, but need to be continued
                Invoke(); //Then lets continue it
        }
        //Wait some time, before next event entry will be activated
        public void WaitForSeconds(float delay)
        {
            curDelay = delay;
        }
    }
}
