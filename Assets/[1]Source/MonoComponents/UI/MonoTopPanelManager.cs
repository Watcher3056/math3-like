﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using System;
using Beebyte.Obfuscator;

namespace Test.Math3Like.Source
{
    public class MonoTopPanelManager : MonoCached
    {
        [Serializable]
        public class Panel
        {
            [Required]
            public MonoPanel panel;
        }
        public List<Panel> panels = new List<Panel>();

        public int currentPanelIndex = -1;

        protected override void Setup()
        {
            HideAllPanels(true);
        }
        protected override void HandleEnable()
        {
            HideAllPanels(true);
        }
        [SkipRename] //Skip rename of this method, else we get problems on build. 
        //You must did it for all methods you would to use trough editor(in buttons, events etc.)
        public void ChangePanelTo(MonoPanel to)
        {
            ChangePanelTo(panels.FindIndex((x) => x.panel == to));
        }
        [SkipRename] //Skip rename of this method, else we get problems on build. 
        //You must did it for all methods you would to use trough editor(in buttons, events etc.)
        public void ChangePanelTo(int to)
        {
            if (to == currentPanelIndex)
                return;
            if (currentPanelIndex >= 0)
            {
                //Trigger panel close
                panels[currentPanelIndex].panel.OnPanelClose.Invoke();
            }

            currentPanelIndex = to;

            if (currentPanelIndex >= 0)
            {
                //Activate panel, cause it can be disabled before
                panels[currentPanelIndex].panel.gameObject.SetActive(true);
                //Trigger panel open
                panels[currentPanelIndex].panel.OnPanelOpen.Invoke();
            }
        }
        [SkipRename]
        public void HideAllPanels(bool instantly)
        {
            ChangePanelTo(-1);
            if (instantly)
                foreach (Panel panel in panels)
                    panel.panel.gameObject.SetActive(false);
        }
    }
}