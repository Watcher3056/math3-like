﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;
using UnityEngine;
using Sirenix.Serialization;

namespace Test.Math3Like.Source
{
    public class MonoPanel : MonoCached
    {
        public enum FadeMode { In, Out }

        [Required]
        public Animator animator;
        public BetterEvent OnPanelOpen;
        public BetterEvent OnPanelClose;

        [ReadOnly]
        public string panelFadeIn = "Panel In";
        [ReadOnly]
        public string panelFadeOut = "Panel Out";

        protected override void Setup()
        {
            RectTransform rt = GetComponent<RectTransform>();
            if (rt != null)
                rt.anchoredPosition = Vector3.zero;
        }

        [SkipRename] //Skip rename of this method, else we get problems on build. 
        //You must did it for all methods you would to use trough editor(in buttons, events etc.)
        public void PanelFade(FadeMode fadeMode)
        {
            animator.Play(fadeMode == FadeMode.In ? panelFadeIn : panelFadeOut);
        }
        [SkipRename]
        public void TogglePanel(bool arg)
        {
            if (arg)
                OpenPanel();
            else
                ClosePanel();
        }
        [SkipRename] //Skip rename of this method, else we get problems on build. 
        //You must did it for all methods you would to use trough editor(in buttons, events etc.)
        public void OpenPanel()
        {
            OnPanelOpen.Invoke();
        }
        [SkipRename] //Skip rename of this method, else we get problems on build. 
        //You must did it for all methods you would to use trough editor(in buttons, events etc.)
        public void ClosePanel()
        {
            OnPanelClose.Invoke();
        }
    }
}
