﻿using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Test.Math3Like.Source
{
    public class MonoScoreCounter : MonoCached, ITick
    {
        public TextMeshProUGUI textTotalScore;
        public TextMeshProUGUI textCurScore;
        public Image imageScoresProgressBar;
        protected override void HandleEnable()
        {
            ProcessorUpdate.Add(this);
        }
        protected override void HandleDisable()
        {
            ProcessorUpdate.Remove(this);
        }
        public void Tick(float delta)
        {
            if (textTotalScore)
                textTotalScore.text = DataGameMain.Default.PlayerScores.ToString();
            if (textCurScore)
                textCurScore.text = ProcessorGameBoard.Default.CurScores.ToString() 
                    + " / " + DataGameMain.Default.requireScores.ToString();
            if (imageScoresProgressBar)
                imageScoresProgressBar.fillAmount = 
                    Mathf.Clamp(
                        (float)ProcessorGameBoard.Default.CurScores / (float)DataGameMain.Default.requireScores,
                        0f, 1f);
        }
    }
}
