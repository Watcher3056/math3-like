﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beebyte.Obfuscator;
using DG.Tweening;
using Pixeye.Actors;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Test.Math3Like.Source
{
    public class MonoLoadingScreen : MonoCached
    {
        public static MonoLoadingScreen Default => _default;
        private static MonoLoadingScreen _default;

        [Required]
        public MonoPanel panel;
        public float closeDelay;

        private bool loadingScreenOpened;
        public MonoLoadingScreen()
        {
            _default = this;
        }
        protected override void Setup()
        {
            ProcessorScene.Default.OnSceneClose = () => HandleSceneLoading(0f);
            ProcessorScene.Default.OnSceneLoading = HandleSceneLoading;
        }
        private void HandleSceneLoading(float progress)
        {
            if (progress < 1f && !loadingScreenOpened)
            {
                panel.OpenPanel();
                loadingScreenOpened = true;
            }
            else if (progress == 1f)
            {
                StartCoroutine(CloseWithDelay(closeDelay));
            }
            this.Log("Scene Loading Progress: " + Mathf.CeilToInt(progress * 100) + "%");
        }
        IEnumerator CloseWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            panel.ClosePanel();
            loadingScreenOpened = false;
        }
    }
}
