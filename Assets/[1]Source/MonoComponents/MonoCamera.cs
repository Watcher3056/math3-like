﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Pixeye.Actors;
using Sirenix.OdinInspector;


namespace Test.Math3Like.Source
{
    public class MonoCamera : MonoCached
    {
        public static MonoCamera Default => _default;
        private static MonoCamera _default;

        [FoldoutGroup("Setup"), Required]
        public new Camera camera;

        public MonoCamera()
        {
            _default = this;
        }
        protected override void Setup()
        {
            
        }
    }
}
