﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pixeye.Actors;
using Sirenix.Serialization;
using UnityEngine;

namespace Test.Math3Like.Source
{
    public partial class ProcessorSaveLoad : Processor, IKernel
    {
        public static ProcessorSaveLoad Default => _default;
        private static ProcessorSaveLoad _default;

        public static event Action OnLocalDataUpdated = () => { };

        private static readonly string path = Application.persistentDataPath + "/Saves/";
        private static readonly string fileNameLocal = "saves.json";
        private static readonly string tagVersion = "SaveFileVersion";
        private static readonly string tagUserID = "UserID";
        //Starts from 0
        private static readonly int lastVersion = 0;

        private static Dictionary<string, object> dicGameData;
        private bool alreadyInProcess;
        private bool dataWasLoadedFromCloud;

        public ProcessorSaveLoad()
        {
            _default = this;
            Application.wantsToQuit += () => { SaveAll(); return true; };
            // Test if Save Folder exists
            if (!Directory.Exists(path))
            {
                if (File.Exists(path))
                    File.Delete(path);
                // Create Save Folder
                Directory.CreateDirectory(path);
            }
            LoadGameData();
            SetupMigration();
            TryMigrate();
        }
        protected override void OnDispose()
        {
            SaveAll();
        }
        public static void SaveAll(bool migrate = false)
        {
            Default.SaveDataLocal(dicGameData, migrate);
        }
        public static void SavePP<T>(string key, T value)
        {
            ES2.Save(value, key, new ES2Settings { saveLocation = ES2Settings.SaveLocation.PlayerPrefs });
        }
        public static T LoadPP<T>(string key, T defaultValue)
        {
            return ExistsPP(key) ? ES2.Load<T>(key, new ES2Settings { saveLocation = ES2Settings.SaveLocation.PlayerPrefs }) : defaultValue;
        }
        public static bool ExistsPP(string key)
        {
            return ES2.Exists(key, new ES2Settings { saveLocation = ES2Settings.SaveLocation.PlayerPrefs });
        }
        public static void Save<T>(string key, T value)
        {
            if (Exists(key))
                dicGameData[key] = value;
            else
                dicGameData.Add(key, value);
        }
        public static T Load<T>(string key)
        {
            try
            {
                return (T)dicGameData[key];
            }
            catch (Exception ex)
            {
                Default.LogError(ex.Message);
                return default(T);
            }
        }
        public static T Load<T>(string key, T defaultValue)
        {
            if (Exists(key))
                return Load<T>(key);
            else
                return defaultValue;
        }
        public static bool Exists(string key)
        {
            return dicGameData.ContainsKey(key);
        }
        public static void CleanSaves()
        {
            File.Delete(path + fileNameLocal);
            PlayerPrefs.DeleteAll();
            Default.Log("Local saves was deleted!");
        }

        private void LoadGameData()
        {
            if (!File.Exists(path + fileNameLocal))
                File.Create(path + fileNameLocal).Dispose();
            dicGameData = GetLocalData(path + fileNameLocal);

            if (dicGameData == null)
                dicGameData = new Dictionary<string, object>();
            else
            {
                List<string> keys = dicGameData.Keys.ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    object value = dicGameData[keys[i]];
                    if (value.GetType() == typeof(Int64))
                        value = Convert.ToInt32((Int64)value);
                    dicGameData[keys[i]] = value;
                }
            }
        }

        #region Helpers
        private static byte[] GameDataToBytes(object gameData)
        {
            return SerializationUtility.SerializeValue(gameData, DataFormat.Binary);
        }
        private static Dictionary<string, object> BytesToGameData(byte[] gameData)
        {
            return SerializationUtility.DeserializeValue<Dictionary<string, object>>(gameData, DataFormat.JSON);
        }
        private void SaveDataLocal(Dictionary<string, object> data, bool migrate = true)
        {
            this.Log("Overwriting local data start...");


            string jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
            File.WriteAllText(path + fileNameLocal, jsonData);


            this.Log("Overwriting local data end!");
            if (migrate)
                TryMigrate();
        }
        private Dictionary<string, object> GetLocalData(string path)
        {
            string jsonData = File.ReadAllText(path);
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData);

            return data;
        }
        #endregion /Saved Games
    }
}
