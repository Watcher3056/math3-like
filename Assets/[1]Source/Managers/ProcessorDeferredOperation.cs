﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using Pixeye.Actors;

namespace Test.Math3Like.Source
{
    public class ProcessorDeferredOperation : Processor, ITick
    {
        private class Item
        {
            public Action action;
            public bool triggerOnce;
            public bool isUnscaledDelta;
            public float triggerPeriod;

            public float curPeriodBuffer;
        }

        public static ProcessorDeferredOperation Default => _default;
        private static ProcessorDeferredOperation _default;

        private List<Item> items;

        public ProcessorDeferredOperation()
        {
            _default = this;

            items = new List<Item>();
        }
        protected override void OnDispose()
        {
            
        }
        public void Tick(float delta)
        {
            float unscaledDeltaTime = Time.unscaledDeltaTime;
            for (int i = 0; i < items.Count; i++)
            {
                try
                {
                    if (items[i].triggerPeriod > items[i].curPeriodBuffer)
                    {
                        items[i].curPeriodBuffer += items[i].isUnscaledDelta ? unscaledDeltaTime : delta;
                        continue;
                    }
                    else
                        items[i].curPeriodBuffer = 0f;
                    items[i].action.Invoke();
                    if (items[i].triggerOnce)
                    {
                        items.RemoveAt(i);
                        i--;
                    }
                }
                catch (Exception ex)
                {
                    this.LogError(ex.Message);
                    items.RemoveAt(i);
                    i--;
                }
            }
        }
        public void Add(Action action, bool triggerOnce = false, bool isUnscaledDelta = false, float triggerPeriod = 0f)
        {
            items.Add(new Item { action = action, triggerOnce = triggerOnce, isUnscaledDelta = isUnscaledDelta, triggerPeriod = triggerPeriod });
        }
        public void Remove(Action action)
        {
            items.Remove(items.Find((x) => { return x.action == action; }));
        }
    }
}
