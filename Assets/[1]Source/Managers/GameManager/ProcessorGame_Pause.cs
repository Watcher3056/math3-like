﻿using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Math3Like.Source
{
    public partial class ProcessorGame
    {
        public static event Action OnPause = () => { };
        private void SetupStatePause()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStatePause;
            state.OnEnd = EndStatePause;

            statesMap.Add(GameState.Pause, state);
        }
        private void StartStatePause()
        {
            MonoGameUI.Default.UpdateView();
            ProcessorUpdate.Remove(ProcessorGameBoard.Default);

            OnPause.Invoke();
        }
        public void EndStatePause()
        {
            ProcessorUpdate.Add(ProcessorGameBoard.Default);
        }
    }
}
