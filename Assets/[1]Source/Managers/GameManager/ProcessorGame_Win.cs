﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Math3Like.Source
{
    public partial class ProcessorGame
    {
        public static event Action OnWin = () => { };
        private void SetupStateWin()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStateWin;
            state.OnEnd = EndStateWin;

            statesMap.Add(GameState.Win, state);
        }
        private void StartStateWin()
        {
            MonoGameUI.Default.UpdateView();

            OnWin.Invoke();
        }
        public void EndStateWin()
        {

        }
    }
}
