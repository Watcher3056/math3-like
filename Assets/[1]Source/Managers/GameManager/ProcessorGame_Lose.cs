﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Math3Like.Source
{
    public partial class ProcessorGame
    {
        public static event Action OnLose = () => { };
        private void SetupStateLose()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStateLose;
            state.OnEnd = EndStateLose;

            statesMap.Add(GameState.Lose, state);
        }
        private void StartStateLose()
        {
            MonoGameUI.Default.UpdateView();

            OnLose.Invoke();
        }
        public void EndStateLose()
        {

        }
    }
}
