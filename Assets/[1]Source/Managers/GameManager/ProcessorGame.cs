﻿using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Test.Math3Like.Source
{
    public partial class ProcessorGame : Processor, ITick
    {
        public static ProcessorGame Default => _default;
        private static ProcessorGame _default;

        public enum GameState { None, Play, Pause, Win, Lose }
        public Dictionary<GameState, StateDefault> statesMap = new Dictionary<GameState, StateDefault>();
        public GameState CurState { get { return curState; } set { curState = value; CheckState(); } }
        private GameState curState = GameState.Play;
        private GameState PrevState { get; set; }

        private AudioSource audioSourceBGM;

        public ProcessorGame()
        {
            ProcessorSaveLoad.OnLocalDataUpdated += HandleLocalDataUpdated;
            _default = this;

            audioSourceBGM = ProcessorSoundPool.Default.PlaySound(DataGameMain.Default.clipsOnPlay.Random(), 1f);
            audioSourceBGM.loop = true;

            SetupStatePlay();
            SetupStatePause();
            SetupStateWin();
            SetupStateLose();
        }
        protected override void OnDispose()
        {
            ProcessorSaveLoad.OnLocalDataUpdated -= HandleLocalDataUpdated;
        }
        public void Tick(float delta)
        {
            CheckState();
        }
        public void Restart()
        {
            ProcessorGameBoard.Default.Restart();
            CurState = GameState.Play;
        }
        private void CheckState()
        {
            if (PrevState != CurState)
            {
                this.Log(" CurState: " + CurState + " PrevState: " + PrevState);
                if (statesMap.ContainsKey(CurState) &&
                    statesMap[CurState].Condition((int)PrevState))
                {
                    if (statesMap.ContainsKey(PrevState))
                        statesMap[PrevState].OnEnd();
                    statesMap[CurState].OnStart();

                    PrevState = CurState;
                }
                else
                    CurState = PrevState;
            }
        }
        private void HandleLocalDataUpdated()
        {

        }
    }
}
