﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Math3Like.Source
{
    public partial class ProcessorGame
    {
        public static event Action OnPlay = () => { };
        private void SetupStatePlay()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStatePlay;
            state.OnEnd = EndStatePlay;

            statesMap.Add(GameState.Play, state);
        }
        private void StartStatePlay()
        {
            MonoGameUI.Default.UpdateView();

            OnPlay.Invoke();
        }
        public void EndStatePlay()
        {

        }
    }
}
