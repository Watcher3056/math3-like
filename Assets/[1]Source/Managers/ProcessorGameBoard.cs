﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pixeye.Actors;
using UnityEngine;
using DG.Tweening;

namespace Test.Math3Like.Source
{
    public class ProcessorGameBoard : Processor, ITick
    {
        [Serializable]
        public class Settings
        {
            public List<ActorBoardItem> prefabBoardItems;
            public Transform transformHolder;
            public int minLineCombo;
            public int gridWidth;
            public int gridHeight;
            public float gridCellSize;
            public float spacing;
            public float moveSpeed;
        }
        public class GridCell
        {
            //Element which contained in cell
            public ent eBoardItem;
            public Vector3 position;
        }

        public static ProcessorGameBoard Default => _default;
        private static ProcessorGameBoard _default;

        public int CurScores
        {
            get { return curScores; }
            set
            {
                DataGameMain.Default.PlayerScores += value - curScores;
                curScores = value;
            }
        }
        private int curScores;
        public Settings settings;
        public Group<ComponentBoardItem> gBoardItems;

        private List<List<GridCell>> grid;
        private List<ent> destructionQueue = new List<ent>();
        private ent eSelectedBoardItem;
        private float curMoveTime;
        public ProcessorGameBoard()
        {
            _default = this;
        }
        public void Setup(Settings settings)
        {
            this.settings = settings;

            DOTween.SetTweensCapacity(500, 50);
            Create2DGrid();
        }
        public void Tick(float delta)
        {
            if (ProcessorGame.Default.CurState != ProcessorGame.GameState.Play)
                return;
            ProcessSpawnItems();
            ProcessItemsFall(delta);
            if (curMoveTime <= 0 && destructionQueue.Count == 0)
                ProcessCheckMath();
            if (curMoveTime <= 0 && destructionQueue.Count == 0)
            {
                if (CheckWin())
                {
                    ProcessorGame.Default.CurState = ProcessorGame.GameState.Win;
                }
                else if (CheckLose())
                {
                    this.Log("Lose");
                }
                ProcessPlayerInput();
            }
        }
        public override void HandleEvents()
        {
            foreach (ent e in gBoardItems.removed)
            {
                destructionQueue.Remove(e);
            }
        }
        public void Restart()
        {
            List<ent> temp = new List<ent>(100);
            foreach (ent eBoardItem in gBoardItems)
                temp.Add(eBoardItem);
            DestroyItems(false, temp.ToArray());

            curScores = 0;
        }
        private void ProcessPlayerInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit = Physics2D.Raycast(
                        MonoCamera.Default.camera.ScreenToWorldPoint(Input.mousePosition),
                        Vector2.zero, Mathf.Infinity);

                ent eNearestBoardItem = FindNearestBoardItem(hit.point);

                if (Vector2.Distance(hit.point, eNearestBoardItem.transform.position) <= settings.gridCellSize)
                {
                    ComponentBoardItem cBoardItemNear = eNearestBoardItem.ComponentBoardItem();
                    ComponentBoardItem cBoardItemSel = eSelectedBoardItem.ComponentBoardItem();
                    if (eSelectedBoardItem.exist)
                    {

                        int differenceX = Mathf.Abs(cBoardItemNear.x - cBoardItemSel.x);
                        int differenceY = Mathf.Abs(cBoardItemNear.y - cBoardItemSel.y);

                        if ((differenceX == 1 && differenceY == 0) || (differenceY == 1 && differenceX == 0))
                        {
                            SwapItems(eSelectedBoardItem, eNearestBoardItem, cBoardItemSel, cBoardItemNear);

                            if (!ProcessCheckMath(false))
                            {
                                ent temp = eSelectedBoardItem;
                                ProcessorDeferredOperation.Default.Add(() =>
                                {
                                    SwapItems(temp, eNearestBoardItem, cBoardItemSel, cBoardItemNear);
                                }, true, false, curMoveTime);
                            }
                        }
                        cBoardItemSel.animator.SetBool(ComponentBoardItem.AnimKeyBoolSelected, false);
                    }

                    cBoardItemNear.animator.SetBool(ComponentBoardItem.AnimKeyBoolSelected, true);
                    eSelectedBoardItem = eNearestBoardItem;
                }
            }
        }
        private bool CheckLose()
        {
            int possibleTurns = 0;
            int maxLineCombo = 0;
            List<ent> lineCombo = new List<ent>();
            string prevId = String.Empty;
            bool passOneGap = true;

            //Check Horizontal
            for (int y = 0; y < settings.gridHeight; y++)
            {
                prevId = String.Empty;
                passOneGap = true;

                for (int x = 0; x < settings.gridWidth; x++)
                {
                    ComponentGUID cGUID = grid[y][x].eBoardItem.ComponentGUID();
                    if (cGUID.id == prevId || x == 0)
                        lineCombo.Add(grid[y][x].eBoardItem);
                    else if (cGUID.id != prevId && lineCombo.Count < settings.minLineCombo)
                    {
                        if (passOneGap &&
                            ((y + 1 < settings.gridHeight && grid[y + 1][x].eBoardItem.ComponentGUID().id == prevId) ||
                             (y - 1 > 0 && grid[y - 1][x].eBoardItem.ComponentGUID().id == prevId)))
                        {
                            passOneGap = false;
                        }
                        else
                            lineCombo.Clear();
                        lineCombo.Add(grid[y][x].eBoardItem);
                    }
                    if (cGUID.id != prevId || x + 1 == settings.gridWidth)
                        if (lineCombo.Count >= settings.minLineCombo)
                        {
                            if (lineCombo.Count > maxLineCombo)
                                maxLineCombo = lineCombo.Count;
                            lineCombo.Clear();
                            lineCombo.Add(grid[y][x].eBoardItem);
                            possibleTurns++;
                        }
                    prevId = cGUID.id;
                }
                lineCombo.Clear();
            }
            //Check Vertical
            for (int x = 0; x < settings.gridWidth; x++)
            {
                passOneGap = true;
                prevId = String.Empty;

                for (int y = 0; y < settings.gridHeight; y++)
                {
                    ComponentGUID cGUID = grid[y][x].eBoardItem.ComponentGUID();
                    if (cGUID.id == prevId || y == 0)
                        lineCombo.Add(grid[y][x].eBoardItem);
                    else if (cGUID.id != prevId && lineCombo.Count < settings.minLineCombo)
                    {
                        if (passOneGap &&
                            ((x + 1 < settings.gridWidth && grid[y][x + 1].eBoardItem.ComponentGUID().id == prevId) ||
                             (x - 1 > 0 && grid[y][x - 1].eBoardItem.ComponentGUID().id == prevId)))
                        {
                            passOneGap = false;
                        }
                        else
                            lineCombo.Clear();
                        lineCombo.Add(grid[y][x].eBoardItem);
                    }
                    if (cGUID.id != prevId || y + 1 == settings.gridHeight)
                        if (lineCombo.Count >= settings.minLineCombo)
                        {
                            if (lineCombo.Count > maxLineCombo)
                                maxLineCombo = lineCombo.Count;
                            lineCombo.Clear();
                            lineCombo.Add(grid[y][x].eBoardItem);
                            possibleTurns++;
                        }
                    prevId = cGUID.id;
                }
                lineCombo.Clear();
            }

            this.Log("Possible Turns Detected: " + possibleTurns);
            return maxLineCombo < settings.minLineCombo;
        }
        private bool CheckWin()
        {
            return curScores >= DataGameMain.Default.requireScores;
        }
        private void ProcessItemsFall(float delta)
        {
            curMoveTime -= delta;
            for (int y = 0; y < settings.gridHeight + 1; y++)
            {
                for (int x = 0; x < settings.gridWidth; x++)
                {
                    ent eBoardItem = grid[y][x].eBoardItem;
                    if (!eBoardItem.exist)
                        continue;
                    ComponentBoardItem cBoardItem = eBoardItem.ComponentBoardItem();
                    int indexY = cBoardItem.y - 1;
                    if (indexY >= 0 && !grid[indexY][cBoardItem.x].eBoardItem.exist)
                    {
                        grid[cBoardItem.y][cBoardItem.x].eBoardItem = 0;
                        grid[indexY][cBoardItem.x].eBoardItem = eBoardItem;

                        curMoveTime = 1f / settings.moveSpeed;

                        cBoardItem.y = indexY;
                        eBoardItem.transform.DOLocalMoveY(grid[indexY][cBoardItem.x].position.y, curMoveTime);
                    }
                }
            }
        }
        private void ProcessSpawnItems()
        {
            for (int x = 0; x < settings.gridWidth; x++)
            {
                if (!grid[settings.gridHeight - 1][x].eBoardItem.exist)
                {
                    CreateItem(x);
                }
            }
        }
        private bool ProcessCheckMath(bool destroyMath = true)
        {
            List<ent> toDestroy = new List<ent>();
            List<ent> lineCombo = new List<ent>();
            string prevId = String.Empty;

            //Check Horizontal
            for (int y = 0; y < settings.gridHeight; y++)
            {
                prevId = String.Empty;
                for (int x = 0; x < settings.gridWidth; x++)
                {
                    ComponentGUID cGUID = grid[y][x].eBoardItem.ComponentGUID();
                    if (cGUID.id == prevId || x == 0)
                        lineCombo.Add(grid[y][x].eBoardItem);
                    else if (cGUID.id != prevId && lineCombo.Count < settings.minLineCombo)
                    {
                        lineCombo.Clear();
                        lineCombo.Add(grid[y][x].eBoardItem);
                    }
                    if (cGUID.id != prevId || x + 1 == settings.gridWidth)
                        if (lineCombo.Count >= settings.minLineCombo)
                        {
                            toDestroy.AddRange(lineCombo.ToArray());
                            lineCombo.Clear();
                            lineCombo.Add(grid[y][x].eBoardItem);
                        }
                    prevId = cGUID.id;
                }
                lineCombo.Clear();
            }
            //Check Vertical
            for (int x = 0; x < settings.gridWidth; x++)
            {
                prevId = String.Empty;

                for (int y = 0; y < settings.gridHeight; y++)
                {
                    ComponentGUID cGUID = grid[y][x].eBoardItem.ComponentGUID();
                    if (cGUID.id == prevId || y == 0)
                        lineCombo.Add(grid[y][x].eBoardItem);
                    else if (cGUID.id != prevId && lineCombo.Count < settings.minLineCombo)
                    {
                        lineCombo.Clear();
                        lineCombo.Add(grid[y][x].eBoardItem);
                    }
                    if (cGUID.id != prevId || y + 1 == settings.gridHeight)
                        if (lineCombo.Count >= settings.minLineCombo)
                        {
                            toDestroy.AddRange(lineCombo.ToArray());
                            lineCombo.Clear();
                            lineCombo.Add(grid[y][x].eBoardItem);
                        }
                    prevId = cGUID.id;
                }
                lineCombo.Clear();
            }


            if (toDestroy.Count > 0)
            {
                ProcessorSoundPool.Default.PlaySound(DataGameMain.Default.clipsOnCombo.Random());
            }
            if (destroyMath)
                DestroyItems(true, toDestroy.ToArray());
            return toDestroy.Count > 0;
        }
        private void DestroyItems(bool addScores, params ent[] eBoardItems)
        {
            foreach (ent eBoardItem in eBoardItems)
            {
                if (destructionQueue.Contains(eBoardItem))
                    continue;
                if (addScores)
                    CurScores++;
                float animDestroyTime = 0f;

                ComponentBoardItem cBoardItem = eBoardItem.ComponentBoardItem();

                cBoardItem.animator.SetTrigger(ComponentBoardItem.AnimKeyTriggerDestroy);
                animDestroyTime = cBoardItem.animator.GetCurrentAnimatorStateInfo(0).length;

                ent _eToDestroy = eBoardItem;

                GameObject.Instantiate(DataGameMain.Default.prefabParticleItemBurst, _eToDestroy.transform.position + Vector3.back, default);
                ProcessorDeferredOperation.Default.Add(() =>
                {
                    _eToDestroy.Release();
                }, true, false, animDestroyTime);
                destructionQueue.Add(_eToDestroy);
            }
        }
        private void SwapItems(ent eA, ent eB, ComponentBoardItem cA, ComponentBoardItem cB)
        {
            GridCell cellA = grid[cA.y][cA.x];
            GridCell cellB = grid[cB.y][cB.x];
            cellA.eBoardItem = eB;
            cellB.eBoardItem = eA;

            int x, y;
            x = cA.x;
            y = cA.y;

            cA.x = cB.x;
            cA.y = cB.y;
            cB.x = x;
            cB.y = y;

            float time = 1f / settings.moveSpeed;
            curMoveTime = time;
            eA.transform.DOLocalMove(cellB.position, time);
            eB.transform.DOLocalMove(cellA.position, time);
        }
        private void Create2DGrid()
        {
            Vector3 startPoint =
                new Vector3(
                    settings.transformHolder.position.x - (settings.gridWidth - 1) * (settings.gridCellSize + settings.spacing),
                    settings.transformHolder.position.y - (settings.gridHeight - 1) * (settings.gridCellSize + settings.spacing));
            startPoint = startPoint / 2f;

            grid = new List<List<GridCell>>(settings.gridHeight + 1);
            float posX, posY;

            for (int y = 0; y < settings.gridHeight + 1; y++)
            {
                posY = y * (settings.gridCellSize + settings.spacing);
                grid.Add(new List<GridCell>(settings.gridWidth));

                for (int x = 0; x < settings.gridWidth; x++)
                {
                    posX = x * (settings.gridCellSize + settings.spacing);
                    grid[y].Add(new GridCell { position = new Vector3(posX, posY) + startPoint });
                }

            }
        }
        private void CreateItem(int posX)
        {
            int posY = settings.gridHeight - 1;

            grid[posY][posX].eBoardItem =
            Actor.Create(settings.prefabBoardItems[UnityEngine.Random.Range(0, settings.prefabBoardItems.Count)].gameObject,
            settings.transformHolder,
            grid[posY][posX].position).entity;

            ComponentBoardItem cBoardItem = grid[posY][posX].eBoardItem.ComponentBoardItem();
            cBoardItem.x = posX;
            cBoardItem.y = posY;
        }
        private ent FindNearestBoardItem(Vector2 mainPoint)
        {
            ent eNearest = 0;
            float minDistance = float.MaxValue;
            float curDistance = 0f;
            foreach (ent e in gBoardItems)
            {
                curDistance = Vector2.Distance(mainPoint, e.transform.position);
                if (curDistance < minDistance)
                {
                    minDistance = curDistance;
                    eNearest = e;
                }
            }
            return eNearest;
        }
    }
}
