﻿using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Test.Math3Like.Source
{
    class ProcessorSoundPool : Processor, ITick
    {
        public static ProcessorSoundPool Default => _default;
        private static ProcessorSoundPool _default;
        public class SoundPoolItem
        {
            public AudioSource audioSource;
            public float idleTime;
        }

        //private Transform poolHolder;
        private int maxIdleTime = 300;
        private int minPoolSize = 5;

        private List<SoundPoolItem> soundPool = new List<SoundPoolItem>(20);

        public ProcessorSoundPool()
        {
            _default = this;
        }
        protected override void OnDispose()
        {
            ClearPull();
        }
        public void Tick(float delta)
        {
            for (int i = 0; i < soundPool.Count; i++)
            {
                if (!soundPool[i].audioSource.isPlaying)
                {
                    soundPool[i].idleTime += UnityEngine.Time.unscaledDeltaTime;
                    if (soundPool[i].idleTime > maxIdleTime && soundPool.Count > minPoolSize)
                    {
                        DestroyPoolItem(soundPool[i]);
                        i--;
                    }
                }
                else
                    soundPool[i].idleTime = 0f;
            }
        }
        public void ClearPull()
        {
            while (soundPool.Count != 0)
                DestroyPoolItem(soundPool[0]);
        }
        public void PlaySound(AudioClip clip)
        {
            PlaySound(clip, 1f);
        }
        public AudioSource PlaySound(AudioClip clip, float volume = 1f)
        {
            SoundPoolItem pool = GetPool();
            pool.audioSource.clip = clip;
            pool.audioSource.loop = false;
            pool.audioSource.volume = volume;
            pool.audioSource.Play();
            return pool.audioSource;
        }
        private SoundPoolItem GetPool()
        {
            for (int i = 0; i < soundPool.Count; i++)
            {
                if (!soundPool[i].audioSource.isPlaying)
                    return soundPool[i];
            }
            SoundPoolItem poolItem = new SoundPoolItem();
            GameObject go = new GameObject("Sound Pool Item");
            go.transform.SetParent(MonoCamera.Default.camera.transform);

            poolItem.audioSource = go.AddComponent<AudioSource>();
            soundPool.Add(poolItem);
            return poolItem;
        }
        private void DestroyPoolItem(SoundPoolItem item)
        {
            GameObject.Destroy(item.audioSource.gameObject);
            item.audioSource = null;
            item.idleTime = 0f;
            soundPool.Remove(item);
        }
    }
}
