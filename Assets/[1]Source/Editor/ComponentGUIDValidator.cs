﻿using System;
using System.Reflection;
using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector.Editor.Validation;
using Sirenix.Utilities;
using UnityEngine;
using Pixeye.Actors;
using UnityEditor;

[assembly: RegisterValidator(typeof(Test.Math3Like.Source.Editor.ComponentGUIDValidator))]
namespace Test.Math3Like.Source.Editor
{

#pragma warning disable
    public class ComponentGUIDValidator : Validator
    {
        public override RevalidationCriteria RevalidationCriteria => RevalidationCriteria.OnValueChange;
        public override bool CanValidateValues()
        {
            return true;
        }
        public override bool CanValidateMembers()
        {
            return false;
        }
        public override bool CanValidateMember(MemberInfo member, Type memberValueType)
        {
            return false;
        }
        public override void RunValueValidation(object value, UnityEngine.Object root, ref ValidationResult result)
        {
            if (result == null)
                result = new ValidationResult();

            result.Setup = new ValidationSetup()
            {
                Kind = ValidationKind.Value,
                Validator = this,
                Value = value,
                Root = root,
            };

            result.ResultType = ValidationResultType.Valid;
            result.Message = "";

            ComponentGUID cGUID = value as ComponentGUID;
            if (cGUID == null)
            {
                result.ResultType = ValidationResultType.IgnoreResult;
                return;
            }
            //else if (cGUID.id != string.Empty && cGUID.id != null)
            //{
            //    result.ResultType = ValidationResultType.IgnoreResult;
            //    return;
            //}

            Actor actor = root as Actor;
            string assetPath = AssetDatabase.GetAssetPath(root);
            if (actor != null)
            {
                assetPath = 
                    UnityEditor.Experimental.SceneManagement.
                    PrefabStageUtility.GetPrefabStage(actor.transform.root.gameObject)?.prefabAssetPath;
                if (assetPath == string.Empty || assetPath == null)
                {
                    try
                    {
                    assetPath = 
                        AssetDatabase.GetAssetPath(
                            PrefabUtility.GetCorrespondingObjectFromSource(
                                PrefabUtility.GetOutermostPrefabInstanceRoot(actor.gameObject)));
                    }
                    catch
                    {
                        result.ResultType = ValidationResultType.IgnoreResult;
                        return;
                    }
                }
            }
            cGUID.id = UnityEditor.AssetDatabase.AssetPathToGUID(assetPath);
            result.ResultType = ValidationResultType.Valid;
        }
    }
#pragma warning enable
}
