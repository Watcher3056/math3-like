﻿using Pixeye.Actors;
using Sirenix.OdinInspector;

namespace Test.Math3Like.Source
{

	public class StarterGame : Starter
	{
        [FoldoutGroup("Setup"), HideLabel, Title("Settings Game Board")]
        public ProcessorGameBoard.Settings settingsGameBoard;
        // use this method to provide processors and initializing stuff.
        protected override void Setup()
		{
            Toolbox.Add<ProcessorDeferredOperation>();
            Toolbox.Add<ProcessorSoundPool>();
            Toolbox.Add<ProcessorGame>();
            Toolbox.Add<ProcessorGameBoard>().Setup(settingsGameBoard);
		}

		// use thos method to perform "cleanup" before scene dies.
		protected override void Dispose()
		{
			// clear buffer when the scene is removed
		}
	}
}