﻿using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Math3Like.Source
{
    public class StarterMainMenu : Starter
    {
        //Main Application Entry
        protected override void Setup()
        {
            Toolbox.Add<ProcessorDebug>();
            Toolbox.Add<ProcessorDeferredOperation>();
            Toolbox.Add<ProcessorSaveLoad>();
            Toolbox.Add<ProcessorSoundPool>();
            Toolbox.Add<ProcessorOrientation>();
        }
        protected override void Dispose()
        {
            
        }
    }
}
