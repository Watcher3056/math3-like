﻿using Pixeye.Actors;
using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Test.Math3Like.Source
{
    [Serializable]
    public class ComponentBoardItem
    {
        //Settings
        [Required]
        public Animator animator;

        //Runtime only
        [ReadOnly]
        public int x;
        [ReadOnly]
        public int y;

        //Anim Keys
        [ReadOnly]
        public const string AnimKeyTriggerDestroy = "Destroy";
        [ReadOnly]
        public const string AnimKeyBoolSelected = "Selected";
    }

    #region HELPERS
    static partial class Component
    {
        public const string BoardItem = "Test.Math3Like.Source.ComponentBoardItem";

        public static ref ComponentBoardItem ComponentBoardItem(in this ent entity) =>
        ref Storage<ComponentBoardItem>.components[entity.id];
    }

    sealed class StorageComponentBoardItem : Storage<ComponentBoardItem>
    {
        public override ComponentBoardItem Create() => new ComponentBoardItem();


        // Use for cleaning components that were removed at the current frame.
        public override void Dispose(indexes disposed)
        {
            foreach (var id in disposed)
            {
                ref var component = ref components[id];
            }
        }
    }
    #endregion
}




