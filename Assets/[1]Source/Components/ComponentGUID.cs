﻿using Pixeye.Actors;
using Sirenix.OdinInspector;
using System;
using UnityEngine;


namespace Test.Math3Like.Source
{
    [Serializable]
    public class ComponentGUID
    {
        [Required, ReadOnly]
        public string id;

        public Guid GetGuid()
        {
            return new Guid(id);
        }
    }

    #region HELPERS
    static partial class Component
    {
        public const string GUID = "Test.Math3Like.Source.ComponentGUID";

        public static ref ComponentGUID ComponentGUID(in this ent entity) =>
        ref Storage<ComponentGUID>.components[entity.id];
    }

    sealed class StorageComponentGUID : Storage<ComponentGUID>
    {
        public override ComponentGUID Create() => new ComponentGUID();


        // Use for cleaning components that were removed at the current frame.
        public override void Dispose(indexes disposed)
        {
            foreach (var id in disposed)
            {
                ref var component = ref components[id];
            }
        }
    }
    #endregion
}




