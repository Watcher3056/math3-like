﻿using Pixeye.Actors;
using Sirenix.OdinInspector;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Test.Math3Like.Source
{
    [Serializable]
    public class ComponentTransform
    {
        [Required]
        public Transform transform;
    }

    #region HELPERS
    static partial class Component
    {
        public const string Transform = "Test.Math3Like.Source.ComponentTransform";

        public static ref ComponentTransform ComponentTransform(in this ent entity) =>
        ref Storage<ComponentTransform>.components[entity.id];
    }

    sealed class StorageComponentTransform : Storage<ComponentTransform>
    {
        public override ComponentTransform Create() => new ComponentTransform();


        // Use for cleaning components that were removed at the current frame.
        public override void Dispose(indexes disposed)
        {
            foreach (var id in disposed)
            {
                ref var component = ref components[id];
            }
        }
    }
    #endregion
}




