﻿using Pixeye.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Test.Math3Like.Source
{
    public class DataGameMain : Pluggable
    {
        #region SAVE LOAD KEYS
        public const string keyPlayerScores = "PlayerScoresRecord";
        #endregion
        #region RUNTIME DATA
        public static DataGameMain Default => _default;
        private static DataGameMain _default;


        public int PlayerScores
        {
            set
            {
                playerScores = value;
                ProcessorSaveLoad.Save(keyPlayerScores, value);
            }
            get
            {
                return playerScores;
            }
        }
        private int playerScores;
        #endregion
        #region SETTINGS
        public int requireScores;

        public ParticleSystem prefabParticleItemBurst;
        public List<AudioClip> clipsOnCombo;
        public List<AudioClip> clipsOnMainMenu;
        public List<AudioClip> clipsOnPlay;
        #endregion


        public override void Plug()
        {
            _default = this;
            ProcessorSaveLoad.OnLocalDataUpdated += HandleLocalDataUpdated;
        }
        private void HandleLocalDataUpdated()
        {
            playerScores = ProcessorSaveLoad.Load(keyPlayerScores, 0);
        }
    }
}
